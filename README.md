# CloudFormation Auto Deploy with Developer Review Environments

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/aws/cloudformation-deploy)

## Implements These GitLab Value Stream Supporting Capabilities

The architectural approach to this code leverages GitLab's [Efficiency](https://about.gitlab.com/handbook/values/#efficiency) and [Results](https://about.gitlab.com/handbook/values/#results) values by building on top of AWS' substantial developer productivity efforts such as their CLIs, automatic IaC generation and mainteance and any abstraction layers designed to improve the developer experience or productivity. This project interfaces with these innovations rather than duplicating them.

- **[Dynamic Review Environments Support](https://docs.gitlab.com/ee/topics/release_your_application.html)** equivalent to that in GitLab's Kubernetes deployments.
    - when a feature branch is forked, a completely isolated environment is created for it.
    - The environment is updated during pushes.
    - The environment torn down when the branch is removed. 
    - **Business Value Stream of Seamless Revew Environments**:
      - Help solve the problem of development friction due to waiting on approval or other teams for new environments.
      - Help solve the problem of environment sprawl in AWS cloud development environments.
      - Sprawl and environment deployment friction get in a negative feedback loop when they are not seamlessly automated.
      - Required to get full value stream benefit of shifting remediation all the way left to the developer's feature branch.
- **Environments Panel** - Environment URLs are exposed via GitLab's environments management page.
- **Live Environment Security Scanning and Checks Support for Feature Branches** - to support DAST, Fuzzing, Accessibiliity and any other shift-left checks that require scanning deployed version of the application. This also enables these findings to be isolated to MRs for feature branch development (the farthest left possible).
- **Auto Deploy / Auto DevOps** - review environments are implemented via an Auto Deploy compliant CI included template that overrides the regular one in Auto DevOps. Even if your organization does not leverage the complete Auto DevOps pipeline, this compatibility ensures interoperation with all of GitLab's other prebuilt pipelines - including linting, security scanning and dynamic review environments.
- **SAST for IaC** - leverages Bridgecrew Checkov to scan IaC. Responds to standard GitLab SAST_DISABLED configuration parameter. Results are parsed as X
- **Linting for IaC** - uses cfn-lint to ensure templates are error free.
- **Test Visualization** - outputs SAST and Linting as JUNIT XML.

### Design Visual
![](glautodeploycfwithdynamicreviewenvironments.png)

Creating a branch automatically creates an isolated CloudFormation stack that is automatically updated as the developer pushes commits. When the developer merges their code into a branch the branch environment is torn down and the environment attached to the merged into branch is updated.

## AWS Best Practices

- **Latest CloudFormation Calls** - leverages "CF Package" to ensure all contents are properly abstracted and built to s3 for execution. Leverages "CF Deploy" to always generate change sets. Optionally receives a parameter to wait for Changeset approval.
- **Monitors CF Stack Status** - for proper completion reporting and for retrieving environment URL from final configuration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2022-01-27

* **GitLab Version Released On**: 14.6

* **GitLab Edition Required**: 

  * For overall solution: (https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:

## Demonstrates These Design Requirements, Desirements and Best Practice

As a complete whole, this guided exploration requires at least the Free edition of GitLab.

* **GitLab Development Pattern :** Consume testing tools outputted junit XML to display test results.
* **Debugging Development Pattern :** CI_SCRIPT_TRACE variable enables scripts to be traced in CI.  If CI_DEBUG_TRACE is on, script tracing also enables.
* * **Debugging Development Pattern :** CI_ENV_TRACE variable prints environment variables at the start of each job - careful that you don't expose secrets.

#### GitLab CI Functionality:

- **.gitlab-ci.yml** "includes" for centrally managed plug-ins can be a file in the same repo, another repo or an https url.
- **.gitlab-ci.yml** "extends" for centrally managed plug-ins  parameter for "template jobs" (gitlab ci custom functions).

### Solution Design Best Practice Features  

- Least config (e.g. Presence of S3_BUCKET_FOR_CFN_TEMPLATES provides data and indicates underlying feature should be enabled)

### DevOps Best Practice Features  

- Processess multiple CloudFormation templates by probing or specified list
- Implements IaC SAST via Checkov (results in junit test panel)
- Implements cfn-lint (results in junit test panel)

### CloudFormation Best Practice Features  

- management of AWS stack name to be unique and identifiable to GitLab source.
- Implements "aws cloudformation **package**" command whenever S3_BUCKET_FOR_CFN_TEMPLATES is specified. Causes CloudFormation to bundle various files up to S3. [aws cloudformation package documentation](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/cloudformation/package.html)
- Implements "cloudformation validate-template" as a proper pre-test[aws cloudformation deploy documentation](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/cloudformation/validate-template.html)
- Implements "aws cloudformation **deploy**" command for simplier code and to always have CF changesets for auditability. [aws cloudformation deploy documentation](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/cloudformation/deploy.html)
- Supports passing [CloudFormation Capabilities](https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_CreateStack.html#API_CreateStack_RequestParameters)
- Builds fails if any template is over CloudFormation Max of 460.8Kb (and needs to be broken out into multiple templates)
- CloudFormation Templates location support
  - Local disk (per CFN requirements => only if size is less than 51.2KB AND "aws cloudformation package" is not needed to bundle other files)
  - S3 location - automatically packaged and uploaded whenever S3_BUCKET_FOR_CFN_TEMPLATES is specified.  **NOTE: Per CFN requirements => Template deployment and bucket must be in the same region.**
  - Error generated if one or more templates are too large for local processing (51.2Kb) and S3_BUCKET_FOR_CFN_TEMPLATES was not specified.

### AutoDevOps Best Practice Features 

- adheres to AutoDevOps variables for disabling SAST and Code Quality
- Supports CloudFormation changeset review before deployment (Configurable to run without review and approval)
- Passes "CF_" prefixed variables through as CloudFormation parameters (prefix is stripped)
- built-in Teardown ("teardown" option omitted for production environment - default branch)
- Implements AutoDevOps style [Review Environments](https://docs.gitlab.com/ee/ci/environments/)
  - Creates new environment (new CF stack) when a branch is created
  - Code / MR can be updated repeatedly
  - Environment is automatically stopped on merge (with branch delete) to another branch

## Using This Pattern
- **REQUIRE_CHANGESET_APPROVALS** - setting this to true causes changesets to be uploaded to artifacts and adds a manual job to apply them after they have been reviewed.  If this is absent, the `aws cloudformation deploy` command will generate changesets and immediately apply them.
- **SAST_DISABLED** - when set to true disables Checkov scan.
- **CODE_QUALITY_DISABLED** - when set to true disables cfn-lint.  `aws cloudformation validate-template` always runs.
- **RESOLVED_TEMPLATE_LIST** - when set to one or more relatively pathed cloudformation templates they are run in the order specified. When not set, a job runs to probe for all files with the mask `*.cf.*` and sorts them in full pathname order to run them. Other important details:
  - You can purposely not include `.cf.` in the file name for nested templates that should not be run directly.
  - When using RESOLVED_TEMPLATE_LIST, the file names do not need to have a specific mask.
-   **S3_BUCKET_FOR_CFN_TEMPLATES** - when specifying a plain bucket name, this causes behavior to shift in the following ways:
  - All templates are prepared with the `aws cloudformation package` command so that bundled files are uploaded to s3.
  - Is required for templates over 51.2Kb in file size because these cannot be used with the local file argument to `aws cloudformation deploy` If you do NOT specify this and one or more templates are over the 51.2Kb limit, the pipeline fails.
  - This is optional because the bucket has to be in the same region the CloudFormation stack will be deployed - so if your templates are small enough and you don't need to use `aws cloudformation package`, then you can deploy to any region with creating and providing a bucket in the region.
  - **S3_KEY_FOR_CFN_TEMPLATES** - if this also exists, the key levels (s3 "directories") are inserted into the final path composition.
- **CFNLINTARGS** - specifies arguments to con-lint, defaults to '--ignore-checks W2' which ignores parameter warnings.
- **CLOUDFORMATION_CAPABILITIES** - allows passing of a space separated list of one or more of CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND.

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/main/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Not A Production Solution

The following disclaimer is emited to the logs by this plugin.  When using it in your own projects, you must copy the repository or it's files and then you can remove the code that emits this statement.

"The original location for this plugin extension is for demonstration and learning purposes only.  There is no breaking change SLA and it may change at any time without notice. If you need to depend on it for production or other critical systems, please make your own isolated copy to depend on.
