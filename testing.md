
#Tests
- run multiple times with no changes (does 'deploy' really not error out?  Do the logs report why there is no action?)
- run a test making a CF or parameter change and see if changeset approval and stack update works
- check if external stack deletion is properly handled by stack teardown stage
- apply changesets then try delete change sets (should do informative graceful exit)
- delete changesets then try apply change sets (should do informative graceful exit)
- with and without change sets enabled
