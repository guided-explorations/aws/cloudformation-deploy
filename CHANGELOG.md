# Changelog

All notable changes to this project will be documented in this file.

## [v1.5.0-alpha2] - 2022-01-27

### Added

- collects environment URL from CloudFormation and exposes in GitLabs `Environments` dashboards
- uses `aws cloudformation package` to prepare template
- uses `aws cloudformation deploy` to create changesets
- always creates changesets for change history purposes - even if these change set review is set to off - in which case the changes are automatically applied.
- creates per-branch s3 keys/folders to prevent clashes when multiple branches are under development at once.


### Updated

- S3 key cleanup occurs between runs and when tearing down

### Known Issues

- If cloud formation jobs exceed runner job timeout - the status tracking will die - need a switch to say not to track.
- CF_ parameter pass through for more than one parameter may not be working

## [v1.4.1-alpha1] - 2021-05-21

### Added

- cfn-tail added to teardown
- change "master" branch to "main"
- enable overriding completed statuses to cfn_tail to enable deleting changesets
- update to collect changesets even when not waiting for them
- REQUIRE_CHANGESET_APPROVALS does true boolean rather than simply existing enabling it - allows self-documented setting of "false" to be used
- fix webserver termination hook monitoring in sample cf template

### Updated

- Status tracking fixed

### Known Issues

- If cloud formation jobs exceed runner job timeout - the status tracking will die - need a switch to say not to track.
- CF_ parameter pass through for more than one parameter may not be working
